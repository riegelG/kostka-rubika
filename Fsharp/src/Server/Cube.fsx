module Model =

    type Wall = Left | Right | Front | Back | Up | Down
    type Color = Red | Blue | White | Orange | Green | Yellow

    type Rotation =
        | Clockwise
        | Counterclockwise

        static member (!!) (r: Rotation) =
            match r with
            | Clockwise -> Counterclockwise
            | Counterclockwise -> Clockwise

    type Piece = Color * Wall

    type Block =
        | Middle of Piece
        | Edge of (Piece * Piece)
        | Corner of (Piece * Piece * Piece)

    type Cube = {
        blocks: Block list
        moves: string list
    }

    type CubeDTO = Map<string, string list>

module CubeBuilder =
    open Model

    let private string2Color (string: string) =
        match string with
        | "red" -> Red
        | "blue" -> Blue
        | "white" -> White
        | "orange" -> Orange
        | "green" -> Green
        | "yellow" -> Yellow
        | _ -> White

    let private string2Wall (string: string) =
        match string with
        | "left" -> Left
        | "right" -> Right
        | "front" -> Front
        | "back" -> Back
        | "up" -> Up
        | "down" -> Down
        | _ -> Front

    let private stringList2LabelList (colors: string list) (wall: Wall) = colors |> List.map(fun s -> Piece(string2Color s, wall))

    let private piecesList2Cube (walls: Map<Wall,Piece list>) : Cube =
        let front = walls.[Front]
        let back = walls.[Back]
        let down = walls.[Down]
        let up = walls.[Up]
        let left = walls.[Left]
        let right = walls.[Right]

        {
        blocks = Middle(front.[4])
        :: Middle(back.[4])
        :: Middle(down.[4])
        :: Middle(left.[4])
        :: Middle(right.[4])
        :: Middle(up.[4])
        :: Corner(front.[0], left.[2], up.[6])
        :: Corner(front.[2], right.[0], up.[8])
        :: Corner(front.[6], left.[8], down.[0])
        :: Corner(front.[8], right.[6], down.[2])
        :: Corner(back.[0], right.[2], up.[2])
        :: Corner(back.[2], left.[0], up.[0])
        :: Corner(back.[6], right.[8], down.[8])
        :: Corner(back.[8], left.[6], down.[6])
        :: Edge(front.[1], up.[7])
        :: Edge(front.[3], left.[5])
        :: Edge(front.[5], right.[3])
        :: Edge(front.[7], down.[1])
        :: Edge(left.[1], up.[3])
        :: Edge(left.[7], down.[3])
        :: Edge(right.[1], up.[5])
        :: Edge(right.[7], down.[5])
        :: Edge(back.[1], up.[1])
        :: Edge(back.[3], right.[5])
        :: Edge(back.[5], left.[3])
        :: Edge(back.[7], down.[1])
        :: [];
        moves = []
        }

    let createCube (cubeDTO: CubeDTO) =
        cubeDTO |> Map.toList
                |> List.map(fun (wall: string, colors: string list) -> (string2Wall wall, stringList2LabelList colors (string2Wall wall)))
                |> Map.ofList
                |> piecesList2Cube

    let private createMonochromeWall(wall: Wall) (color: Color) = (wall, [1..9] |> List.map(fun _ -> Piece (color, wall)))

    let createDefaultCube =
        [
            createMonochromeWall Front White;
            createMonochromeWall Up Orange;
            createMonochromeWall Right Blue;
            createMonochromeWall Left Green;
            createMonochromeWall Down Red;
            createMonochromeWall Back Yellow
        ] |> Map.ofList |> piecesList2Cube
