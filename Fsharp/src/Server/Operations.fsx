open Cube.Model

let axisX: Wall [] = [|Wall.Front; Wall.Up;  Wall.Back; Wall.Down|]
let axisY: Wall [] = [|Wall.Back; Wall.Right; Wall.Front; Wall.Left|]
let axisZ: Wall [] = [|Wall.Up; Wall.Right; Wall.Down; Wall.Left|]

let toPiecesList (block: Block) : Piece list =
    match block with
    | Block.Middle (p) -> [p]
    | Block.Edge (p1, p2) -> [p1; p2]
    | Block.Corner (p1, p2, p3) -> [p1; p2; p3]

let isMiddle (block: Block): bool =
    match block with
    | Block.Middle(_) -> true
    | _ -> false

let isEdge (block: Block): bool =
    match block with
    | Block.Edge(_) -> true
    | _ -> false

let isCorner (block: Block): bool =
    match block with
    | Block.Corner(_) -> true
    | _ -> false

let toBlock (pieces: Piece list) : Block =
    match pieces with
    | [p] -> Block.Middle(p)
    | [p1; p2] -> Block.Edge(p1, p2)
    | p1 :: p2 :: p3 :: _ -> Block.Corner(p1, p2, p3)

let private piecesToColors (pieces: Piece list) = pieces |> List.map (fun (color, _) -> color)

let private piecesToWalls (pieces: Piece list) = pieces |> List.map (fun (_, wall) -> wall)

let private block2Colors (block: Block): Color list =  block |> toPiecesList |> piecesToColors

let private block2Walls (block: Block): Wall list = block |> toPiecesList |> piecesToWalls

let isSameList (list1: 'T list) (list2: 'T list) = List.forall (fun item -> List.contains item list1) list2

let belongsToWall(wall: Wall) (block: Block) : bool =  block |> toPiecesList |> List.exists(fun (_, w) -> w = wall)

let filterBlocksByPiece (piece: Piece) (blocks: Block list): Block list = blocks |> List.filter (fun b -> List.contains piece (toPiecesList b))

let hasPiece (piece: Piece) (block: Block) = toPiecesList block |> List.contains piece

let hasBlockWithPiece (piece: Piece) (blocks: Block list): bool = blocks |> List.exists (fun b -> hasPiece piece b)

let getMiddleColor (blocks: Block list) (wall: Wall): Color = blocks |> List.filter (fun b -> belongsToWall wall b) |> List.find (fun b -> isMiddle b) |> block2Colors |> List.head

let isCorrectPlaced (cube: Cube) (block: Block) =
    let walls = block |> block2Walls
    block |> block2Colors |> List.forall (fun c -> List.exists (fun w -> (getMiddleColor cube.blocks w) = c) walls)

let hasColors (colors: Color list) (block: Block): bool =
    let blockColors = block |> block2Colors
    blockColors.Length = colors.Length && isSameList colors blockColors

let partitionByWall (wall: Wall) (blocks: Block list) : (Block list * Block list) = blocks |> List.partition(fun b -> belongsToWall wall b)

let private rotateLabel((color, direction): Piece) (axis: Wall []) (rotation: Rotation): Piece =
    let next (array: Wall []) (index: int): Wall = array.[(index + 1) % 4]
    let prev (array: Wall []) (index: int): Wall = array.[(index + 3) % 4]

    let rotate (index: int option): Wall =
        match index with
        | None -> direction
        | Some index -> index |> if rotation = Clockwise then next axis else prev axis

    let new_direction = Array.tryFindIndex (fun d -> d = direction) axis |> rotate
    (color, new_direction)

let private rotateBlock(block: Block) (axis: Wall []) (rotation: Rotation): Block =
    block
        |> toPiecesList
        |> List.map(fun l -> rotateLabel l axis rotation)
        |> toBlock

let private addMove (wall: Wall) (rotation: Rotation) (oldMoves: string list):string list =
    let mutable move = match wall with
    | Wall.Left ->  "L"
    | Wall.Right -> "R"
    | Wall.Up -> "U"
    | Wall.Down -> "D"
    | Wall.Front -> "F"
    | Wall.Back -> "B"
    if (rotation = Counterclockwise)
    then oldMoves @ [(move + "'")]
    else oldMoves @ [move]

let private addMoveByAxis (axis: Wall []) (rotation: Rotation) (oldMoves: string list):string list =
    let mutable move = match axis with
    | [|Wall.Front; Wall.Up;  Wall.Back; Wall.Down|] -> "X"
    | [|Wall.Back; Wall.Right; Wall.Front; Wall.Left|] -> "Y"
    | [|Wall.Up; Wall.Right; Wall.Down; Wall.Left|] -> "Z"
    | _ -> "_"
    if (rotation = Counterclockwise)
    then oldMoves @ [(move + "'")]
    else oldMoves @ [move]

let reverseRotation (wall: Wall) = match wall with | Wall.Left | Wall.Down | Wall.Back -> true | _ -> false

let rotateWall (wall: Wall) (axis: Wall []) (rotation: Rotation) (cube: Cube): Cube =
    let (filtred, rest) = partitionByWall wall cube.blocks
    { cube with blocks = (List.map(fun b -> rotateBlock b axis (if reverseRotation wall then !!rotation else rotation)) filtred) @ rest; moves = addMove wall rotation cube.moves}

let rotateCube (axis: Wall []) (rotation: Rotation) (cube: Cube): Cube =
    { cube with blocks = (List.map(fun b -> rotateBlock b axis rotation) cube.blocks); moves = addMoveByAxis axis rotation cube.moves}

let getBlockByColors (colors: Color list) (blocks: Block list): Block = blocks |> List.filter(fun b -> hasColors colors b) |> List.head

let U = rotateWall Wall.Up axisY Clockwise
let U' = rotateWall Wall.Up axisY Counterclockwise

let F = rotateWall Wall.Front axisZ Clockwise
let F' = rotateWall Wall.Front axisZ Counterclockwise

let L = rotateWall Wall.Left axisX Clockwise
let L' = rotateWall Wall.Left axisX Counterclockwise

let R = rotateWall Wall.Right axisX Clockwise
let R' = rotateWall Wall.Right axisX Counterclockwise

let D = rotateWall Wall.Down axisY Clockwise
let D' = rotateWall Wall.Down axisY Counterclockwise

let B = rotateWall Wall.Back axisZ Clockwise
let B' = rotateWall Wall.Back axisZ Counterclockwise

let X = rotateCube axisX Clockwise
let X' = rotateCube axisX Counterclockwise

let Y = rotateCube axisY Clockwise
let Y' = rotateCube axisY Counterclockwise

let Z = rotateCube axisZ Clockwise
let Z' = rotateCube axisZ Counterclockwise