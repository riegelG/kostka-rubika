open System.IO
open System.Threading.Tasks

open System
open Microsoft.AspNetCore.Builder
open Microsoft.Extensions.DependencyInjection
open FSharp.Control.Tasks.V2
open Giraffe
open Saturn
open Shared
open Cube.Model
open Cube.CubeBuilder
open Algorithms.Resolver
open Algorithms.Algorithms

let tryGetEnv key =
    match Environment.GetEnvironmentVariable key with
    | x when String.IsNullOrWhiteSpace x -> None
    | x -> Some x

let publicPath = Path.GetFullPath "../Client/public"

let port =
    "SERVER_PORT"
    |> tryGetEnv |> Option.map uint16 |> Option.defaultValue 8085us

let webApp = router {
    post "/api/resolve" (fun next ctx ->
        task {
            let! cubeDTO = ctx.BindJsonAsync<CubeDTO>()
            let cube = createCube cubeDTO
            let resolvedCube = resolve cube

            return! json resolvedCube.blocks next ctx
        })
    post "/api/mix" (fun next ctx ->
        task {
            let! moves = ctx.BindJsonAsync<Map<string, string list>>()
            let cube = mixCube createDefaultCube moves.["moves"]
            let resolvedCube = resolve cube
            return! json resolvedCube.blocks next ctx
        })
}

let app = application {
    url ("http://0.0.0.0:" + port.ToString() + "/")
    use_router webApp
    memory_cache
    use_static publicPath
    use_json_serializer(Thoth.Json.Giraffe.ThothSerializer())
    use_gzip
}

run app
