open Operations
open Cube.Model

module Algorithms =

    let rec mixCube (cube: Cube) (moves: List<string>): Cube =
        match moves with
        | "R" :: tail -> mixCube (cube |> R) tail
        | "R'" :: tail -> mixCube (cube |> R') tail
        | "L" :: tail -> mixCube (cube |> L) tail
        | "L'" :: tail -> mixCube (cube |> L') tail
        | "U" :: tail -> mixCube (cube |> U) tail
        | "U'" :: tail -> mixCube (cube |> U') tail
        | "D" :: tail -> mixCube (cube |> D) tail
        | "D'" :: tail -> mixCube (cube |> D') tail
        | "F" :: tail -> mixCube (cube |> F) tail
        | "F'" :: tail -> mixCube (cube |> F') tail
        | "B" :: tail -> mixCube (cube |> B) tail
        | "B'" :: tail -> mixCube (cube |> B') tail
        | _ | [] -> {cube with moves = []}


module FrontWall =

    let rec composeBlock (target: Color) (colors: Color list) (cube: Cube): Cube =
        let composeEdge (((_,w1), (_,w2)): (Piece * Piece)) (cube: Cube): Cube =
            match w1, w2 with
            | Front, Up -> cube
            | Front, Left -> cube |> L |> L |> B |> L |> L |> composeBlock target colors
            | Front, Right -> cube |> R |> R |> B |> R |> R |> composeBlock target colors
            | Front, Left -> cube |> D |> D |> B |> D |> D |> composeBlock target colors
            | Up, Back -> cube |> B |> composeBlock target colors
            | Left, Back -> cube |> L |> U' |> L'
            | Right, Back -> cube |> R' |> U |> R
            | Left, Up -> cube |> U'
            | Right, Up -> cube |> U
            | Left, Down -> cube |> F |> F |> D |> F |> F
            | Right, Down -> cube |> F |> F |> D' |> F |> F
            | Left, Front -> cube |> L |> L |> composeBlock target colors
            | Right, Front -> cube |> R |> R |> composeBlock target colors
            | Up, Front -> cube |> U |> U |> composeBlock target colors
            | Down, Front -> cube |> D |> D |> composeBlock target colors
            | Back, Up -> cube |> U |> U
            | Back, _ -> cube |> B |> composeBlock target colors
            | Down, Back -> cube |> B |> composeBlock target colors
            | Up, Right -> cube |> F |> R' |> F'
            | Down, Right -> cube |> F |> R |> F'
            | Up, Left -> cube |> F' |> L |> F
            | Down, Left -> cube |> F' |> L' |> F
            | _ -> cube

        let composeCorner (((_, w1), (_, w2), (_, w3)): (Piece * Piece * Piece)) (cube: Cube): Cube =
            match w1, w2, w3 with
            | Front, Left, Up
            | Front, Up, Left -> cube
            | Front, w1', w2'
            | w1', Front, w2'
            | w1', w2', Front ->
                match w1', w2' with
                | Up, Left | Left, Up -> cube |> U |> B |> U' |> composeBlock target colors
                | Up, Right | Right, Up -> cube |> U' |> B |> U |> composeBlock target colors
                | Down, Left | Left, Down -> cube |> D' |> B |> D |> composeBlock target colors
                | Down, Right | Right, Down -> cube |> D |> B |> D' |> composeBlock target colors
                | _ -> cube
            | Back, w1', w2' ->
                match w1', w2' with
                | Left, Up | Up, Left -> cube |> L' |> B |> L |> composeBlock target colors
                | _ -> cube |> B |> composeBlock target colors
            | w1', Back, w2'
            | w1', w2', Back ->
                match w1', w2' with
                | Up, Left | Left, Down -> cube |> B' |> composeBlock target colors
                | Right, Up -> cube |> L' |> B |> L
                | Down, Right -> cube |> B |> composeBlock target colors
                | Left, Up -> cube |> B |> composeBlock target colors
                | Down, Left -> cube |> U |> B' |> U'
                | Right, Down -> cube |> B' |> composeBlock target colors
                | Up, Right -> cube |> B |> composeBlock target colors
                | _ -> cube
            | _ -> cube

        let block = getBlockByColors colors cube.blocks
        match block with
        | Middle(_) -> cube
        | Edge ((color, _) as p1, p2) when color = target -> composeEdge (p1, p2) cube
        | Edge (p1, ((color, _) as p2)) when color = target -> composeEdge (p2, p1) cube
        | Corner ((color, _) as p1, p2, p3) when color = target -> composeCorner (p1, p2, p3) cube
        | Corner (p1, ((color, _) as p2), p3) when color = target -> composeCorner (p2, p1, p3) cube
        | Corner (p1, p2, ((color, _) as p3)) when color = target -> composeCorner (p3, p2, p1) cube


    let rec composeFrontEdges (frontColor: Color) (axis: Wall list) (cube: Cube): Cube =
      match axis with
      | [] -> cube
      | wall::tail ->
            let color = getMiddleColor cube.blocks wall
            composeFrontEdges frontColor tail ((composeBlock frontColor [frontColor; color] cube) |> F')

    let rec composeFrontCorners (frontColor: Color) (axis: Wall list) (cube: Cube) =
      match axis with
      | [_]-> cube
      | wall :: ((wall2 :: _) as tail) ->
            let color1 = getMiddleColor cube.blocks wall
            let color2 = getMiddleColor cube.blocks wall2
            composeFrontCorners frontColor tail ((composeBlock frontColor [frontColor; color1; color2] cube) |> F')

    let composeFrontWall (cube: Cube): Cube =
        let frontColor = getMiddleColor cube.blocks Wall.Front
        cube
           |> composeFrontEdges frontColor (axisZ |> Array.toList)
           |> composeFrontCorners frontColor (axisZ.[3] :: (axisZ |> Array.toList))

module SecondLine =

    let private toRight (cube: Cube) = cube |> U |> R |> U' |> R' |> U' |> F' |> U |> F
    let private toLeft (cube: Cube) = cube |> U' |> L' |> U |> L |> U |> F |> U' |> F'
    let private leftRotate (cube: Cube) = cube |> toLeft |> U |> U |> toLeft
    let private rightRotate (cube: Cube) = cube |> toRight |> U |> U |> toRight

    let rec private removeLeft (colors: Color list) (cube: Cube)  =
        let block = getBlockByColors colors cube.blocks
        match block with
        | Edge ((color, Wall.Front), (color2, Wall.Left))
        | Edge ((color2, Wall.Left), (color, Wall.Front)) when (color = colors.[0] && color2 = colors.[1]) || (color = colors.[1] && color2 = colors.[0]) -> cube |> toLeft
        | _ -> cube |> Y' |> removeLeft colors |> Y

    let rec private removeRight (colors: Color list) (cube: Cube)  =
        let block = getBlockByColors colors cube.blocks
        match block with
        | Edge ((color, Wall.Front), (color2, Wall.Right)) | Edge ((color2, Wall.Right), (color, Wall.Front)) when (color = colors.[0] && color2 = colors.[1]) || (color = colors.[1] && color2 = colors.[0]) -> cube |> toRight
        | _ -> cube |> Y |> removeRight colors |> Y'

    let rec private composeLeftEdge (cube: Cube): Cube =
        let front = getMiddleColor cube.blocks Wall.Front
        let left = getMiddleColor cube.blocks Wall.Left
        let block = getBlockByColors [front; left] cube.blocks
        match block with
        | Edge ((color, Wall.Front), (color2, Wall.Left)) | Edge ((color2, Wall.Left), (color, Wall.Front)) when color = front && color2 = left -> cube
        | Edge ((color, Wall.Front), (color2, Wall.Left)) | Edge ((color2, Wall.Left), (color, Wall.Front)) when color2 = front && color = left -> cube |> leftRotate
        | Edge ((color, Wall.Front), (_, Wall.Up)) | Edge ((_, Wall.Up), (color, Wall.Front)) when color = front -> cube |> toLeft
        | Edge (_, (color, Wall.Up)) | Edge ((color, Wall.Up), _) when color <> front -> cube |> U |> composeLeftEdge
        | Edge (_, (color, Wall.Up)) | Edge ((color, Wall.Up), _) when color = front -> cube |> Y' |> composeRightEdge |> Y
        | _ -> cube |> removeLeft [front; left] |> composeLeftEdge
    and private composeRightEdge (cube: Cube): Cube =
        let front = getMiddleColor cube.blocks Wall.Front
        let right = getMiddleColor cube.blocks Wall.Right
        let block = getBlockByColors [front; right] cube.blocks
        match block with
        | Edge ((color, Wall.Front), (color2, Wall.Right)) | Edge ((color2, Wall.Right), (color, Wall.Front)) when color = front && color2 = right -> cube
        | Edge ((color, Wall.Front), (color2, Wall.Right)) | Edge ((color2, Wall.Right), (color, Wall.Front)) when color2 = front && color = right -> cube |> rightRotate
        | Edge ((color, Wall.Front), (_, Wall.Up)) | Edge ((_, Wall.Up), (color, Wall.Front)) when color = front -> cube |> toRight
        | Edge (_, (color, Wall.Up)) | Edge ((color, Wall.Up), _) when color <> front -> cube |> U |> composeRightEdge
        | Edge (_, (color, Wall.Up)) | Edge ((color, Wall.Up), _) when color = front -> cube |> Y |> composeLeftEdge |> Y'
        | _ -> cube |> removeRight [front; right] |> composeRightEdge

    let composeSecondLine (cube: Cube): Cube =
        cube
            |> X'
            |> composeLeftEdge
            |> composeRightEdge
            |> Y
            |> Y
            |> composeLeftEdge
            |> composeRightEdge

module TopCross =

    let isHorizontalLine (b1: Block) (b2: Block): bool = (belongsToWall Wall.Left b1 && belongsToWall Wall.Right b2) || (belongsToWall Wall.Left b2 && belongsToWall Wall.Right b1)
    let isVerticalLine (b1: Block) (b2: Block) = (belongsToWall Wall.Back b1 && belongsToWall Wall.Front b2) || (belongsToWall Wall.Back b2 && belongsToWall Wall.Front b1)
    let is9oclock (b1: Block) (b2: Block) = (belongsToWall Wall.Left b1 && belongsToWall Wall.Back b2) || (belongsToWall Wall.Left b2 && belongsToWall Wall.Back b1)
    let is3oclock (b1: Block) (b2: Block) = (belongsToWall Wall.Right b1 && belongsToWall Wall.Back b2) || (belongsToWall Wall.Right b2 && belongsToWall Wall.Back b1)
    let isHalfTo4oclock (b1: Block) (b2: Block) = (belongsToWall Wall.Right b1 && belongsToWall Wall.Front b2) || (belongsToWall Wall.Right b2 && belongsToWall Wall.Front b1)
    let isHalfTo10oclock (b1: Block) (b2: Block) = (belongsToWall Wall.Left b1 && belongsToWall Wall.Front b2) || (belongsToWall Wall.Left b2 && belongsToWall Wall.Front b1)

    let hasCross (blocks: Block list) = List.length blocks = 4

    let toCross (cube: Cube) = cube |> F |> R |> U |> R' |> U' |> F'

    let rec composeTopCross (cube: Cube) =
        let topMiddleColor = getMiddleColor cube.blocks Wall.Up
        let blocks = cube.blocks |> filterBlocksByPiece (Piece (topMiddleColor, Wall.Up)) |> List.filter (fun b -> isEdge b)
        match blocks with
        | _ when hasCross blocks -> cube
        | b1 :: b2 :: _ when isHorizontalLine b1 b2 -> cube |> toCross
        | b1 :: b2 :: _ when isVerticalLine b1 b2 -> cube |> Y |> composeTopCross
        | b1 :: b2 :: _ when is9oclock b1 b2 -> cube |> toCross |> composeTopCross
        | b1 :: b2 :: _ when is3oclock b1 b2 -> cube |> Y |> composeTopCross
        | b1 :: b2 :: _ when isHalfTo4oclock b1 b2 -> cube |> Y |> Y |> composeTopCross
        | b1 :: b2 :: _ when isHalfTo10oclock b1 b2 -> cube |> Y' |> composeTopCross
        | _ -> cube |> toCross |> composeTopCross


module CorrectTopCross =

    let private algorithm (cube: Cube): Cube = cube |> R |> U |> R' |> U |> R |> U |> U |> R' |> U

    let private hasCorrectCross (left: Color) (right: Color) (front: Color) (back: Color) (cross: Block list): bool = hasBlockWithPiece (Piece (left, Wall.Left)) cross &&
        hasBlockWithPiece (Piece (right, Wall.Right)) cross &&
        hasBlockWithPiece (Piece (front, Wall.Front)) cross &&
        hasBlockWithPiece (Piece (back, Wall.Back)) cross

    let hasCorrectRightBack (right: Color) (back: Color) (cross: Block list) =
        ((hasBlockWithPiece (Piece (right, Wall.Right)) cross) && (hasBlockWithPiece (Piece (back, Wall.Back)) cross))

    let hasCorrectLeftRight (left: Color) (right: Color) (cross: Block list) =
        (hasBlockWithPiece (Piece (right, Wall.Right)) cross) && (hasBlockWithPiece (Piece (left, Wall.Right)) cross)

    let hasLine (left: Color) (right: Color) (front: Color) (back: Color) (cross: Block list) =
        (hasBlockWithPiece (Piece (left, Wall.Left)) cross && hasBlockWithPiece (Piece (right, Wall.Right)) cross)
        || (hasBlockWithPiece (Piece (front, Wall.Front)) cross && hasBlockWithPiece (Piece (back, Wall.Back)) cross)

    let hasRightBack (left: Color) (right: Color) (front: Color) (back: Color) (cross: Block list) =
        ((hasBlockWithPiece (Piece (left, Wall.Left)) cross) && (hasBlockWithPiece (Piece (back, Wall.Back)) cross))
        || ((hasBlockWithPiece (Piece (front, Wall.Front)) cross) && (hasBlockWithPiece (Piece (left, Wall.Left)) cross))
        || ((hasBlockWithPiece (Piece (front, Wall.Front)) cross) && (hasBlockWithPiece (Piece (right, Wall.Left)) cross))

    let rec composeCorrectTopCross (acc: int) (cube: Cube) =
        let topMiddleColor = getMiddleColor cube.blocks Wall.Up
        let left = getMiddleColor cube.blocks Wall.Left
        let right = getMiddleColor cube.blocks Wall.Right
        let front = getMiddleColor cube.blocks Wall.Front
        let back = getMiddleColor cube.blocks Wall.Back
        let blocks = cube.blocks |> filterBlocksByPiece (Piece (topMiddleColor, Wall.Up)) |> List.filter (fun b -> isEdge b)
        match blocks with
        | _ when hasCorrectCross left right front back blocks -> cube
        | _ when hasCorrectRightBack right back blocks -> cube |> algorithm |> composeCorrectTopCross 0
        | _ when hasRightBack left right front back blocks -> cube |> Y |> composeCorrectTopCross 0
        | _ when hasCorrectLeftRight left right blocks -> cube |> algorithm |> composeCorrectTopCross 0
        | _ when hasLine left right front back blocks -> cube |> Y |> composeCorrectTopCross 0
        | _ when acc = 4 -> cube |> algorithm |> composeCorrectTopCross 0
        | _ ->  cube |> U |> composeCorrectTopCross (acc + 1)

module Corners =
    let moveCorners (cube: Cube): Cube = cube |> U |> R |> U' |> L' |> U |> R' |> U' |> L
    let rotateCorner (cube: Cube): Cube = cube |> D |> R' |> D' |> R

    let private getRightFrontCorner (block: Block list): Block = block |> List.filter (fun b -> (belongsToWall Wall.Right b) && (belongsToWall Wall.Front b)) |> List.head

    let allCornersOnCorrectPlace (cube: Cube) (corners: Block list) =
        corners |> List.forall (fun b -> b |> isCorrectPlaced cube)

    let allCornersCorrectRotated (cube: Cube) (corners: Block list) =
        let top = getMiddleColor cube.blocks Wall.Up
        corners |> List.forall (fun b -> hasPiece (Piece (top, Wall.Up)) b)

    let checkRightFrontCorner (cube: Cube) (corners: Block list) =
        corners
            |> getRightFrontCorner
            |> isCorrectPlaced cube

    let checkOtherCorners (cube: Cube) (corners: Block list) =
        (corners |> List.filter (fun b -> (belongsToWall Wall.Right b) && (belongsToWall Wall.Back b)) |> List.head |> isCorrectPlaced cube)
        || (corners |> List.filter (fun b -> (belongsToWall Wall.Left b) && (belongsToWall Wall.Back b)) |> List.head |> isCorrectPlaced cube)
        || (corners |> List.filter (fun b -> (belongsToWall Wall.Left b) && (belongsToWall Wall.Front b)) |> List.head |> isCorrectPlaced cube)


    let rec composeCorners (cube: Cube) =
        let top = getMiddleColor cube.blocks Wall.Up
        let right = getMiddleColor cube.blocks Wall.Right
        let front = getMiddleColor cube.blocks Wall.Front
        let (filtred, _) = partitionByWall Wall.Up cube.blocks
        let corners = filtred |> List.filter (fun b -> isCorner b)
        match corners with
        | _ when allCornersOnCorrectPlace cube corners && allCornersCorrectRotated cube corners -> cube
        | _ when allCornersOnCorrectPlace cube corners -> cube |> rotateCorners
        | _ when checkRightFrontCorner cube corners -> cube |> moveCorners |> composeCorners
        | _ when checkOtherCorners cube corners -> cube |> Y |> composeCorners
        | _ -> cube |> moveCorners |> composeCorners
    and
        rotateCorners (cube: Cube) =
            let (filtred, _) = partitionByWall Wall.Up cube.blocks
            let corners = filtred |> List.filter (fun b -> isCorner b)
            let top = getMiddleColor cube.blocks Wall.Up
            let rightFrontCorner = getRightFrontCorner cube.blocks
            match rightFrontCorner with
            | _ when allCornersOnCorrectPlace cube corners && allCornersCorrectRotated cube corners -> cube
            | _ when hasPiece (Piece (top, Wall.Up)) rightFrontCorner -> cube |> U |> rotateCorners
            | _ -> cube |> rotateCorner |> rotateCorners

module Resolver =
    open FrontWall
    open SecondLine
    open TopCross
    open CorrectTopCross
    open Corners

    let resolve (cube: Cube): Cube =
        cube
            |> composeFrontWall
            |> composeSecondLine
            |> composeTopCross
            |> composeCorrectTopCross 0
            |> composeCorners
