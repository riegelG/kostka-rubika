﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using RubiksCubeCs.Models;
using RubiksCubeCs.Services;

namespace RubiksCubeCs.Controllers
{
    [ApiController]
    public class CubeController : ControllerBase
    {

        [HttpPost]
        [Route("api/cube/resolve")]
        public List<String> resolve([FromBody] string mix)
        {
            List<String> mixMoves = mix.Split(',').ToList(); 
            Cube cube = new CubeBuilder()
                                .BuildCube()
                                .DefaultWalls()
                                .GetCube();
            cube.Mix(mixMoves);

            var fronWall = new FrontWallService(cube);
            var secondLine = new SecondLineService(cube);
            var topCross = new TopCrossService(cube);
            var corners = new CornersService(cube);

            fronWall.composeFrontWall();
            secondLine.compose();
            topCross.composeCross();
            topCross.composeCorrectCross();
            corners.composeCorners();
            return cube.GetMoves();
        }
    }
}
