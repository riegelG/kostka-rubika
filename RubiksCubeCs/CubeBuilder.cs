﻿using RubiksCubeCs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RubiksCubeCs
{
    public class CubeBuilder
    {
        private Cube cube = new Cube();

        enum Corner
        {
            FrontLeftUp,
            FrontRightUp,
            FrontLeftDown,
            FrontRightDown,
            BackLeftUp,
            BackRightUp,
            BackLeftDown,
            BackRightDown
        }

        enum Edge
        {
            FrontUp,
            FrontRight,
            FrontDown,
            FrontLeft,
            LeftUp,
            LeftDown,
            RightUp,
            RightDown,
            BackUp,
            LeftBack,
            BackDown,
            RightBack
        }

        private CornerBlock[] cornerBlocks = new CornerBlock[8];
        private EdgeBlock[] edgeBlocks = new EdgeBlock[12];

        private readonly Corner[] frontCorners = { Corner.FrontLeftUp, Corner.FrontRightUp, Corner.FrontLeftDown, Corner.FrontRightDown };
        private readonly Corner[] leftCorners = { Corner.BackLeftUp, Corner.FrontLeftUp, Corner.BackLeftDown, Corner.FrontLeftDown };
        private readonly Corner[] rightCorners = { Corner.FrontRightUp, Corner.BackRightUp, Corner.FrontRightDown, Corner.BackRightDown };
        private readonly Corner[] backCorners = { Corner.BackRightUp, Corner.BackLeftUp, Corner.BackRightDown, Corner.BackLeftDown };
        private readonly Corner[] upCorners = { Corner.BackLeftUp, Corner.BackRightUp, Corner.FrontLeftUp, Corner.FrontRightUp };
        private readonly Corner[] downCorners = { Corner.FrontLeftDown, Corner.FrontRightDown, Corner.BackLeftDown, Corner.BackRightDown };

        private readonly Edge[] frontEdges = { Edge.FrontUp, Edge.FrontRight, Edge.FrontDown, Edge.FrontLeft };
        private readonly Edge[] leftEdges = { Edge.LeftUp, Edge.FrontLeft, Edge.LeftDown, Edge.LeftBack };
        private readonly Edge[] rightEdges = { Edge.RightUp, Edge.RightBack, Edge.RightDown, Edge.FrontRight };
        private readonly Edge[] backEdges = { Edge.BackUp, Edge.LeftBack, Edge.BackDown, Edge.RightBack };
        private readonly Edge[] upEdges = { Edge.BackUp, Edge.RightUp, Edge.FrontUp, Edge.LeftUp};
        private readonly Edge[] downEdges = { Edge.FrontDown, Edge.RightDown, Edge.BackDown, Edge.LeftDown};

        public CubeBuilder BuildCube()           
        {       
            for (int i = 0; i < 12; i++)
            {
                if (i < 8)
                {
                    cornerBlocks[i] = new CornerBlock();
                }
                edgeBlocks[i] = new EdgeBlock();
            }

            BuildWall(Cube.WallType.Front, frontCorners, frontEdges);
            BuildWall(Cube.WallType.Left, leftCorners, leftEdges);
            BuildWall(Cube.WallType.Right, rightCorners, rightEdges);
            BuildWall(Cube.WallType.Back, backCorners, backEdges);
            BuildWall(Cube.WallType.Up, upCorners, upEdges);
            BuildWall(Cube.WallType.Down, downCorners, downEdges);

            return this;
        }

        public Cube GetCube()
        {
            return cube;
        }

        public CubeBuilder ColorWall(Cube.WallType position, Label.Color[] colors)
        {
            Label.Color middleColor = colors[4];
            List<Label.Color> colorsWithoutMiddle = colors.ToList();
            colorsWithoutMiddle.RemoveAt(4);

            var wall = cube.GetWall(position);
            wall.ColorBlocks(colorsWithoutMiddle.ToArray(), middleColor);
            return this;
        }

        private void BuildWall(Cube.WallType position, Corner[] corners, Edge[] edges)
        {
            Wall wall = new Wall(position);
            wall.AddCorners(
                cornerBlocks[(int)corners[0]],
                cornerBlocks[(int)corners[1]],
                cornerBlocks[(int)corners[2]],
                cornerBlocks[(int)corners[3]]);
            wall.AddEdges(
                edgeBlocks[(int)edges[0]],
                edgeBlocks[(int)edges[1]],
                edgeBlocks[(int)edges[2]],
                edgeBlocks[(int)edges[3]]);
            cube.AddWall(wall, position);
        }

        public CubeBuilder DefaultWalls()
        {
            WallInColor(Cube.WallType.Front, Label.Color.Blue);
            WallInColor(Cube.WallType.Left, Label.Color.Orange);
            WallInColor(Cube.WallType.Right, Label.Color.Red);
            WallInColor(Cube.WallType.Back, Label.Color.Green);
            WallInColor(Cube.WallType.Up, Label.Color.Yellow);
            WallInColor(Cube.WallType.Down, Label.Color.White);
            return this;
        }

        public CubeBuilder WallInColor(Cube.WallType position, Label.Color color)
        {
            Label.Color[] colors = new Label.Color[9];
            for (int i = 0; i < colors.Count(); i++)
            {
                colors[i] = color;
            }

            ColorWall(position, colors);
            return this;
        }
    }
}
