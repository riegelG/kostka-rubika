﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace RubiksCubeCs.Models
{
    public class Label
    {
        public enum Color { White, Blue, Green, Red, Orange, Yellow }
        public readonly Color color;
        public Cube.WallType position;

        public Label(Color color, Cube.WallType position)
        {
            this.position = position;
            this.color = color;
        }

        public void ChangePosition(Cube.WallType newPosition)
        {
            position = newPosition;
        }


    }

    public abstract class Block
    {
        abstract protected int MAX_LABELS { get; }

        protected List<Label> labels = new List<Label>();

        public void AddLabel(Label label)
        {
            if (labels.Count < MAX_LABELS && !hasColor(label.color))
            {
                labels.Add(label);
            } 
        }

        public bool hasColor(Label.Color color)
        {
            return labels.Where(label => label.color == color).Any();
        }

        public bool HasColors(Label.Color[] colors)
        {
            return labels.All(label => colors.Contains(label.color)) && colors.Length == MAX_LABELS;
        }

        public List<Label> GetLabels()
        {
            return labels;
        }

        public List<Cube.WallType> BelongsTo()
        {
            return labels.Select(label => label.position).ToList();
        }

        public void ChangeLabelPositions(Dictionary<Cube.WallType, Cube.WallType> transitions)
        {
            foreach (Label label in labels)
            {
                if (transitions.ContainsKey(label.position))
                {
                    label.ChangePosition(transitions[label.position]);
                }
            }
            
        }
    }

    public class MiddleBlock : Block
    {
        protected override int MAX_LABELS { get => 1; }
    }

    public class EdgeBlock : Block
    {
        protected override int MAX_LABELS { get => 2; }
    }

    public class CornerBlock : Block
    {
        protected override int MAX_LABELS { get => 3; }
    }
}
