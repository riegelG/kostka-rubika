﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Runtime.InteropServices.ComTypes;
using System.Security.Cryptography;
using System.Threading.Tasks;
using static RubiksCubeCs.Models.Label;

namespace RubiksCubeCs.Models
{
    public class Wall
    {
        public enum PositionOnWall { 
            LeftTop = 0, 
            Top = 1,
            RightTop = 2,
            Right = 3,
            RightBottom = 4,
            Bottom = 5,
            LeftBottom = 6,
            Left = 7,
            Center = 8
        }
        private const int MOVING_BLOCKS = 8;
        private const int MAX_BLOCKS = 9;

        private Cube.WallType type;
        private Dictionary<PositionOnWall, Block> blocks = new Dictionary<PositionOnWall, Block>();

        public Wall(Cube.WallType type)
        {
            this.type = type;
            blocks[PositionOnWall.Center] = new MiddleBlock();
        }

        public void AddCorners(CornerBlock leftTop, CornerBlock rightTop, CornerBlock leftBottom, CornerBlock rightBottom)
        {
            blocks[PositionOnWall.LeftTop] = leftTop;
            blocks[PositionOnWall.RightTop] = rightTop;
            blocks[PositionOnWall.LeftBottom] = leftBottom;
            blocks[PositionOnWall.RightBottom] = rightBottom;
        }

        public void AddEdges(EdgeBlock top, EdgeBlock right, EdgeBlock bottom, EdgeBlock left)
        {
            blocks[PositionOnWall.Top] = top;
            blocks[PositionOnWall.Right] = right;
            blocks[PositionOnWall.Bottom] = bottom;
            blocks[PositionOnWall.Left] = left;
        }

        public List<Block> GetBlocks()
        {
            return blocks.Values.ToList();
        }

        public Block GetBlock(PositionOnWall position)
        {
            return blocks[position];
        }

        public Label.Color GetMiddleColor()
        {
            return blocks[PositionOnWall.Center].GetLabels().First().color;
        }

        public void ColorBlocks(Label.Color[] colors, Label.Color middle)
        {
            blocks[PositionOnWall.Center].AddLabel(new Label(middle, type));
            for (int i = 0; i < 3; i++)
            {
                blocks[(PositionOnWall)i].AddLabel(new Label(colors[i], type));
            }
        }

        public List<Block> GetCrossBlocks()
        {
            return (List<Block>) blocks.Values.Where((arg) => arg.GetLabels().Count() == 2);
        }

        public bool is9oclock()
        {
            return blocks[PositionOnWall.Left].GetLabels().Any((arg) => arg.position == type && arg.color == GetMiddleColor())
                && blocks[PositionOnWall.Top].GetLabels().Any((arg) => arg.position == type && arg.color == GetMiddleColor());
        }

        public bool is3oclock()
        {
            return blocks[PositionOnWall.Right].GetLabels().Any((arg) => arg.position == type && arg.color == GetMiddleColor())
                && blocks[PositionOnWall.Top].GetLabels().Any((arg) => arg.position == type && arg.color == GetMiddleColor());
        }

        public bool is3oclockHalf()
        {
            return blocks[PositionOnWall.Right].GetLabels().Any((arg) => arg.position == type && arg.color == GetMiddleColor())
                && blocks[PositionOnWall.Bottom].GetLabels().Any((arg) => arg.position == type && arg.color == GetMiddleColor());
        }

        public bool is9oclockHalf()
        {
            return blocks[PositionOnWall.Left].GetLabels().Any((arg) => arg.position == type && arg.color == GetMiddleColor())
                && blocks[PositionOnWall.Bottom].GetLabels().Any((arg) => arg.position == type && arg.color == GetMiddleColor());
        }

        public bool HasL()
        {
            return is9oclock() || is3oclock() || is3oclockHalf() || is9oclockHalf();
        }

        public bool HasCross()
        {
            return HasHorizontalLine() && HasVerticalLine();
        }

        public bool HasLine()
        {
            return HasHorizontalLine() || HasVerticalLine();
        }

        public bool HasHorizontalLine()
        {
            return blocks[PositionOnWall.Left].GetLabels().Any((arg) => arg.position == type && arg.color == GetMiddleColor())
                && blocks[PositionOnWall.Right].GetLabels().Any((arg) => arg.position == type && arg.color == GetMiddleColor());
        }

        public bool HasVerticalLine()
        {
            return blocks[PositionOnWall.Top].GetLabels().Any((arg) => arg.position == type && arg.color == GetMiddleColor())
                && blocks[PositionOnWall.Bottom].GetLabels().Any((arg) => arg.position == type && arg.color == GetMiddleColor());
        }

        public Block FindBlock(Label.Color[] colors)
        {
            return blocks.Values.Where(block => block.HasColors(colors)).First();
        }

        public List<Tuple<Block, Block>> Rotate()
        {
            var transitions = new List<Tuple<Block, Block>>();
            var labelTransitions = new Dictionary<Cube.WallType, Cube.WallType>();

            Dictionary<PositionOnWall, Block> newState = new Dictionary<PositionOnWall, Block>();
            newState[PositionOnWall.Center] = blocks[PositionOnWall.Center];
            
            for (int i = 0; i < MOVING_BLOCKS; i++)
            {
                var oldPosition = (PositionOnWall)i;
                var newPosition = (PositionOnWall)((i + 2) % MOVING_BLOCKS);

                newState[newPosition] = blocks[oldPosition];

                transitions.Add(new Tuple<Block, Block>(blocks[oldPosition], blocks[newPosition]));
                if (blocks[oldPosition] is EdgeBlock && blocks[oldPosition].BelongsTo().Count() > 0)
                {
                    var beforeLabelPosition = blocks[oldPosition].BelongsTo().Where(wallType => wallType != type).First();
                    var afterLabelPosition = blocks[oldPosition].BelongsTo().Where(wallType => wallType != type).First();

                    labelTransitions.Add(beforeLabelPosition, afterLabelPosition);
                }
            }

            foreach (Block block in newState.Values)
            {
                block.ChangeLabelPositions(labelTransitions);
            }

            blocks = newState;

            return transitions;
        }

        public List<Tuple<Block, Block>> RotatePrim()
        {
            var transitions = new List<Tuple<Block, Block>>();
            var labelTransitions = new Dictionary<Cube.WallType, Cube.WallType>();

            Dictionary<PositionOnWall, Block> newState = new Dictionary<PositionOnWall, Block>();
            newState[PositionOnWall.Center] = blocks[PositionOnWall.Center];

            for (int i = 0; i < MOVING_BLOCKS; i++)
            {
                var oldPosition = (PositionOnWall)i;
                var newPosition = (PositionOnWall) ((MOVING_BLOCKS + (i - 2)) % MOVING_BLOCKS);

                newState[newPosition] = blocks[oldPosition];

                transitions.Add(new Tuple<Block, Block>(blocks[oldPosition], blocks[newPosition]));
                if (blocks[oldPosition] is EdgeBlock && blocks[oldPosition].BelongsTo().Count() > 0)
                {
                    var beforeLabelPosition = blocks[oldPosition].BelongsTo().Where(wallType => wallType != type).First();
                    var afterLabelPosition = blocks[oldPosition].BelongsTo().Where(wallType => wallType != type).First();

                    labelTransitions.Add(beforeLabelPosition, afterLabelPosition);
                }
            }

            foreach (Block block in newState.Values)
            {
                block.ChangeLabelPositions(labelTransitions);
            }

            blocks = newState;

            return transitions;
        }

        public void replaceBlock(Block oldBlock, Block newBlock)
        {
            var positionOldBlock = blocks.FirstOrDefault(x => x.Value == oldBlock).Key;
            blocks[positionOldBlock] = newBlock;
        }
    }
}
