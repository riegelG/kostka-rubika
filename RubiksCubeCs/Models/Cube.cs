﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RubiksCubeCs.Models
{
    public class Cube
    {
        public enum WallType
        {
            Up,
            Left,
            Front,
            Right,
            Down,
            Back
        }

        public WallType[] xAxis = { WallType.Front, WallType.Up, WallType.Back, WallType.Down };
        public WallType[] yAxis = { WallType.Back, WallType.Right, WallType.Front, WallType.Left };
        public WallType[] zAxis = { WallType.Up, WallType.Right, WallType.Down, WallType.Left };

        private Dictionary<WallType, Wall> walls = new Dictionary<WallType, Wall>();
        private String[] moves;

        public void AddWall(Wall wall, WallType position)
        {
            walls[position] = wall;
        }

        public void Mix(List<String> moves)
        {
            foreach (String move in moves)
            {
                switch (move)
                {
                    case "F": F(); break;
                    case "F'": FPrim(); break;
                    case "B": B(); break;
                    case "B'": BPrim(); break;
                    case "L": L(); break;
                    case "L'": LPrim(); break;
                    case "R": R(); break;
                    case "R'": RPrim(); break;
                    case "U": U(); break;
                    case "U'": UPrim(); break;
                    case "D": D(); break;
                    case "D'": DPrim(); break;
                }
            }
            moves.Clear();
        }

        public Cube F()
        {
            moves.Append("F");
            return RotateWall(WallType.Front);
        }

        public Cube FPrim()
        {
            moves.Append("F'");
            return RotateWallPrim(WallType.Front);
        }

        public Cube L()
        {
            moves.Append("L'");
            return RotateWall(WallType.Left);
        }

        public Cube LPrim()
        {
            moves.Append("L'");
            return RotateWallPrim(WallType.Left);
        }

        public Cube R()
        {
            moves.Append("R");
            return RotateWall(WallType.Right);
        }

        public Cube RPrim()
        {
            moves.Append("R'");
            return RotateWallPrim(WallType.Right);
        }

        public Cube B()
        {
            moves.Append("B");
            return RotateWall(WallType.Back);
        }

        public Cube BPrim()
        {
            moves.Append("B'");
            return RotateWallPrim(WallType.Back);
        }

        public Cube U()
        {
            moves.Append("U");
            return RotateWall(WallType.Up);
        }

        public Cube UPrim()
        {
            moves.Append("U'");
            return RotateWallPrim(WallType.Up);
        }

        public Cube D()
        {
            moves.Append("D");
            return RotateWall(WallType.Down);
        }

        public Cube DPrim()
        {
            moves.Append("D'");
            return RotateWallPrim(WallType.Down);
        }

        public Cube X()
        {
            moves.Append("X");
            return RotateCube(xAxis);
        }

        public Cube XPrim()
        {
            moves.Append("X'");
            return RotateCubePrim(xAxis);
        }

        public Cube Z()
        {
            moves.Append("Z");
            return RotateCube(zAxis);
        }

        public Cube ZPrim()
        {
            moves.Append("Z'");
            return RotateCubePrim(zAxis);
        }

        public Cube Y()
        {
            moves.Append("Y");
            return RotateCube(yAxis);
        }

        public Cube YPrim()
        {
            moves.Append("Y'");
            return RotateCubePrim(yAxis);
        }

        public Block FindBlock (Label.Color[] colors)
        {
            foreach (Wall wall in walls.Values)
            {
                Block block = wall.FindBlock(colors);
                if (block != null)
                {
                    return block;
                }
            }
            return null;
        }

        public Label.Color GetWallColor (WallType wallType)
        {
            return walls[wallType].GetMiddleColor();
        }

        public Cube RotateCube(WallType[] axis)
        {
            HashSet<Label> allLabels = new HashSet<Label>();
            foreach (Wall wall in walls.Values)
            {
                var blocks = wall.GetBlocks();
                foreach (Block block in blocks)
                {
                    block.GetLabels().ForEach(l => allLabels.Add(l));
                }
            }
            foreach (Label label in allLabels)
            {
                for (int i = 0; i < axis.Length; i++)
                {
                    if (label.position == axis[i % axis.Length])
                    {
                        label.ChangePosition(axis[(i + 1) % axis.Length]);
                        break;
                    }
                }
            }



            return this;
        }

        public Cube RotateCubePrim(WallType[] axis)
        {
            HashSet<Label> allLabels = new HashSet<Label>();
            foreach (Wall wall in walls.Values)
            {
                var blocks = wall.GetBlocks();
                foreach (Block block in blocks)
                {
                    block.GetLabels().ForEach(l => allLabels.Add(l));
                }
            }
            foreach (Label label in allLabels)
            {
                for (int i = 0; i < axis.Length; i++)
                {
                    if (label.position == axis[i % axis.Length])
                    {
                        label.ChangePosition(axis[(i + axis.Length - 1) % axis.Length]);
                        break;
                    }
                }
            }
            return this;
        }

        private Cube RotateWall(WallType type)
        {
            var transitions = walls[type].Rotate();
            foreach ((var block, var blockOnNewPosition) in transitions)
            {
                foreach (WallType wallType in blockOnNewPosition.BelongsTo())
                {
                    if (wallType != type)
                    {
                        walls[wallType].replaceBlock(blockOnNewPosition, block);
                    }
                }
            }
            return this;
        }

        private Cube RotateWallPrim(WallType type)
        {
            var transitions = walls[type].RotatePrim();
            foreach ((var block, var blockOnNewPosition) in transitions)
            {
                foreach (WallType wallType in block.BelongsTo())
                {
                    if (wallType != type)
                    {
                        walls[wallType].replaceBlock(blockOnNewPosition, block);
                    }

                }
            }
            return this;
        }

        public Wall GetWall(WallType type)
        {
            return walls[type];
        }

        public List<String> GetMoves()
        {
            return moves.ToList();
        }
    }
}
