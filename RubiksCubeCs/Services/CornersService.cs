using System;
using System.Linq;
using RubiksCubeCs.Models;

namespace RubiksCubeCs.Services
{
    public class CornersService
    {
        private Cube cube;

        public CornersService(Cube cube)
        {
            this.cube = cube;
        }

        public void composeCorners()
        {
            Wall topWall = cube.GetWall(Cube.WallType.Up);
            var backColor = cube.GetWallColor(Cube.WallType.Back);
            var leftColor = cube.GetWallColor(Cube.WallType.Left);
            var rightColor = cube.GetWallColor(Cube.WallType.Right);
            var frontColor = cube.GetWallColor(Cube.WallType.Front);

            var fruCorner = isCorrectPlace(topWall.GetBlock(Wall.PositionOnWall.RightBottom), frontColor, rightColor);
            var fluCorner = isCorrectPlace(topWall.GetBlock(Wall.PositionOnWall.LeftBottom), frontColor, leftColor);
            var bluCorner = isCorrectPlace(topWall.GetBlock(Wall.PositionOnWall.LeftTop), backColor, leftColor);
            var bruCorner = isCorrectPlace(topWall.GetBlock(Wall.PositionOnWall.RightTop), backColor, rightColor);

            if (fruCorner && fluCorner && bluCorner && bruCorner)
            {
                checkAndRotateCorners();
                return;
            } else if (fruCorner)
            {
                moveCorners();
                composeCorners();
            }
            else
            {
                cube.U();
                composeCorners();
            }

        }

        public void checkAndRotateCorners()
        {
            Wall topWall = cube.GetWall(Cube.WallType.Up);
            var topColor = topWall.GetMiddleColor();


            var fruCorner = isCorrectRotate(topWall.GetBlock(Wall.PositionOnWall.RightBottom), topColor);
            var fluCorner = isCorrectRotate(topWall.GetBlock(Wall.PositionOnWall.LeftBottom), topColor);
            var bluCorner = isCorrectRotate(topWall.GetBlock(Wall.PositionOnWall.LeftTop), topColor);
            var bruCorner = isCorrectRotate(topWall.GetBlock(Wall.PositionOnWall.RightTop), topColor);

            if (fruCorner && fluCorner && bluCorner && bruCorner)
            {
                return;
            } else if (fruCorner)
            {
                cube.U();
                checkAndRotateCorners();
            } else
            {
                rotateCorners();
                checkAndRotateCorners();
            }

        }

        private bool isCorrectPlace(Block block, Label.Color color1, Label.Color color2)
        {
            return block.hasColor(color1) && block.hasColor(color2);
        }

        private bool isCorrectRotate(Block block, Label.Color color)
        {
            return block.GetLabels().Any(l => l.position == Cube.WallType.Up && l.color == color);
        }

        private void moveCorners()
        {
            cube.U().R().UPrim().LPrim().U().RPrim().UPrim().L();
        }

        private void rotateCorners()
        {
            cube.D().RPrim().DPrim().R();
        }
    }
}
