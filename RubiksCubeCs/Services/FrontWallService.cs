using System;
using System.Collections.Generic;
using System.Linq;
using RubiksCubeCs.Models;

namespace RubiksCubeCs.Services
{
    public class FrontWallService
    {
        private Cube cube;

        public FrontWallService(Cube cube)
        {
            this.cube = cube;
        }

        public Cube composeFrontWall()
        {
            Cube.WallType[] walls = { Cube.WallType.Up, Cube.WallType.Right, Cube.WallType.Down, Cube.WallType.Left };
            Label.Color front = cube.GetWallColor(Cube.WallType.Front);

            foreach (Cube.WallType secondWall in walls)
            {
                Label.Color second = cube.GetWallColor(secondWall);
                Block block = cube.FindBlock(new Label.Color[] { front, second });

                composeEdge(front, second, block);
            }

            for (int i = 0; i < walls.Length; i++)
            {
                Cube.WallType secondWall = walls[i + 4 % 4];
                Cube.WallType thirdWall = walls[(i + 3) % 4];

                Label.Color second = cube.GetWallColor(secondWall);
                Label.Color third = cube.GetWallColor(thirdWall);

                Block block = cube.FindBlock(new Label.Color[] { front, second, third });

                composeCorner(front, second, third, block);
            }

            return cube;
        }

        private void composeEdge(Label.Color front, Label.Color second, Block block)
        {
            List<Label> labels = block.GetLabels();
            
            Cube.WallType frontColorPosition = labels.Where((b) => b.color == front).Select((b) => b.position).First();
            Cube.WallType secondColorPosition = labels.Where((b) => b.color == second).Select((b) => b.position).First();
            
            switch (frontColorPosition)
                {
                case Cube.WallType.Front:
                    switch (secondColorPosition)
                    {
                        case Cube.WallType.Up: return;
                        case Cube.WallType.Left:
                            cube.L().L().B().L().L();
                            break;
                        case Cube.WallType.Right:
                            cube.R().R().B().R().R();
                            break;
                        case Cube.WallType.Down:
                            cube.D().D().B().D().D();
                            break;
                    }
                    break;
                case Cube.WallType.Up:
                    switch (secondColorPosition)
                    {
                        case Cube.WallType.Right:
                            cube.F().RPrim().FPrim();
                            break;
                        case Cube.WallType.Left:
                            cube.FPrim().LPrim().F();
                            break;
                        case Cube.WallType.Back:
                            cube.B();
                            break;
                        case Cube.WallType.Front:
                            cube.U().U();
                            break;
                    }
                    break;
                case Cube.WallType.Left:
                    switch (secondColorPosition)
                    {
                        case Cube.WallType.Back:
                            cube.L().UPrim().LPrim();
                            break;
                        case Cube.WallType.Up:
                            cube.UPrim();
                            break;
                        case Cube.WallType.Down:
                            cube.F().F().D().F().F();
                            break;
                        case Cube.WallType.Front:
                            cube.L().L();
                            break;
                    }
                    break;
                case Cube.WallType.Right:
                    switch (secondColorPosition)
                    {
                        case Cube.WallType.Back:
                            cube.RPrim().U().R();
                            break;
                        case Cube.WallType.Up:
                            cube.U();
                            break;
                        case Cube.WallType.Down:
                            cube.F().F().DPrim().F().F();
                            break;
                        case Cube.WallType.Front:
                            cube.R().R();
                            break;
                    }
                    break;
                case Cube.WallType.Down:
                    switch (secondColorPosition)
                    {
                        case Cube.WallType.Right:
                            cube.F().R().FPrim();
                            break;
                        case Cube.WallType.Left:
                            cube.FPrim().LPrim().F();
                            break;
                        case Cube.WallType.Back:
                            cube.B();
                            break;
                        case Cube.WallType.Front:
                            cube.D().D();
                            break;
                    }
                    break;
                case Cube.WallType.Back:
                    switch (secondColorPosition)
                    {
                        case Cube.WallType.Up:
                            cube.U().U();
                            break;
                        default:
                            cube.B();
                            break;
                    }
                    break;
                default: return;
            }
            
            composeEdge(front, second, block);
        }

        private void composeCorner(Label.Color front, Label.Color second, Label.Color third, Block block)
        {
            List<Label> labels = block.GetLabels();

            Cube.WallType frontColorPosition = labels.Where((b) => b.color == front).Select((b) => b.position).First();
            Cube.WallType secondColorPosition = labels.Where((b) => b.color == second).Select((b) => b.position).First();
            Cube.WallType thirdColorPosition = labels.Where((b) => b.color == third).Select((b) => b.position).First();

            switch (frontColorPosition, secondColorPosition, thirdColorPosition)
            {
                case (Cube.WallType.Front, Cube.WallType.Up, Cube.WallType.Left):
                case (Cube.WallType.Front, Cube.WallType.Left, Cube.WallType.Up):
                    return;
                case (Cube.WallType.Front, _, _):
                    algFrontCorner(secondColorPosition, thirdColorPosition);
                    break;
                case (_, Cube.WallType.Front, _):
                    algFrontCorner(secondColorPosition, thirdColorPosition);
                    break;
                case (_, _, Cube.WallType.Front):
                    algFrontCorner(secondColorPosition, thirdColorPosition);
                    break;
                case (Cube.WallType.Back, _, _):
                    switch (secondColorPosition, thirdColorPosition)
                    {
                        case (Cube.WallType.Up, Cube.WallType.Left):
                        case (Cube.WallType.Left, Cube.WallType.Up):
                            cube.LPrim().B().L();
                            break;
                        default:
                            cube.B();
                            break;
                    }
                    break;
                case (_, Cube.WallType.Back, _):
                    algBackCorner(frontColorPosition, thirdColorPosition);
                    break;
                case (_, _, Cube.WallType.Back):
                    algBackCorner(frontColorPosition, secondColorPosition);
                    break;
                default: return;

            }

            composeCorner(front, second, third, block);
        }

        private void algFrontCorner(Cube.WallType position1, Cube.WallType position2)
        {
            switch (position1, position2)
            {
                case (Cube.WallType.Up, Cube.WallType.Left):
                case (Cube.WallType.Left, Cube.WallType.Up):
                    cube.U().B().UPrim();
                    break;
                case (Cube.WallType.Up, Cube.WallType.Right):
                case (Cube.WallType.Right, Cube.WallType.Up):
                    cube.UPrim().B().U();
                    break;
                case (Cube.WallType.Down, Cube.WallType.Left):
                case (Cube.WallType.Left, Cube.WallType.Down):
                    cube.DPrim().B().D();
                    break;
                case (Cube.WallType.Down, Cube.WallType.Right):
                case (Cube.WallType.Right, Cube.WallType.Down):
                    cube.D().B().DPrim();
                    break;
            }
        }

        private void algBackCorner(Cube.WallType position1, Cube.WallType position2)
        {
            switch (position1, position2)
            {
                case (Cube.WallType.Up, Cube.WallType.Left):
                case (Cube.WallType.Left, Cube.WallType.Down):
                    cube.BPrim();
                    break;
                case (Cube.WallType.Right, Cube.WallType.Up):
                    cube.LPrim().B().L();
                    break;
                case (Cube.WallType.Down, Cube.WallType.Right):
                case (Cube.WallType.Left, Cube.WallType.Up):
                    cube.B();
                    break;
                case (Cube.WallType.Down, Cube.WallType.Left):
                    cube.U().BPrim().UPrim();
                    break;
                case (Cube.WallType.Right, Cube.WallType.Down):
                    cube.BPrim();
                    break;
                case (Cube.WallType.Up, Cube.WallType.Right):
                    cube.B();
                    break;
            }
        }
    }
}
