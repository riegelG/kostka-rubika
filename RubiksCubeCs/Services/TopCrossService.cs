using System;
using System.Linq;
using RubiksCubeCs.Models;

namespace RubiksCubeCs.Services
{
    public class TopCrossService
    {
        private Cube cube;

        public TopCrossService(Cube cube)
        {
            this.cube = cube;
        }

        public void composeCross()
        {
            Wall topWall = cube.GetWall(Cube.WallType.Up);
            Label.Color topColor = cube.GetWallColor(Cube.WallType.Up);

            if (topWall.HasCross())
            {
                return;
            }
            else if (topWall.HasHorizontalLine())
            {
                algorithmToCross();
            }
            else if (topWall.HasVerticalLine())
            {
                cube.Y();
                algorithmToCross();
            }
            else if (topWall.HasL())
            {
                if (topWall.is9oclock())
                {
                    algorithmToCross();
                } else
                {
                    cube.Y();
                    composeCross();
                }
            }
        }

        private void algorithmToCross()
        {
            cube.F().R().U().RPrim().UPrim().FPrim();
        }

        public void composeCorrectCross()
        {
            Wall topWall = cube.GetWall(Cube.WallType.Up);
            var backWallColor = cube.GetWallColor(Cube.WallType.Back);
            var leftWallColor = cube.GetWallColor(Cube.WallType.Left);
            var rightWallColor = cube.GetWallColor(Cube.WallType.Right);
            var frontWallColor = cube.GetWallColor(Cube.WallType.Front);

            var topColor = topWall.GetBlock(Wall.PositionOnWall.Top).GetLabels().Where(b => b.position != Cube.WallType.Up).Select(l => l.color).First();
            var leftColor = topWall.GetBlock(Wall.PositionOnWall.Left).GetLabels().Where(b => b.position != Cube.WallType.Up).Select(l => l.color).First();
            var rightColor = topWall.GetBlock(Wall.PositionOnWall.Right).GetLabels().Where(b => b.position != Cube.WallType.Up).Select(l => l.color).First();
            var bottomColor = topWall.GetBlock(Wall.PositionOnWall.Bottom).GetLabels().Where(b => b.position != Cube.WallType.Up).Select(l => l.color).First();

            var wallColors = new Label.Color[] { leftWallColor, backWallColor, rightWallColor, frontWallColor }; 
            var blockColors = new Label.Color[] { leftColor, topColor, rightColor, bottomColor };

            var match0 = match(wallColors, blockColors, 0);
            var match1 = match(wallColors, blockColors, 1);
            var match2 = match(wallColors, blockColors, 2);
            var match3 = match(wallColors, blockColors, 3);
            var maxMatch = Math.Max(Math.Max(match0, match1), Math.Max(match2, match3));

            if (match0 == 4) {
                return;
            } else if (maxMatch == 4)
            {
                cube.U();
            } else if (maxMatch == 2)
            {
                if (topColor == backWallColor && rightColor == rightWallColor)
                {
                    algorithmToCorrectCross();
                }
                else if (rightColor == rightWallColor && leftColor == leftWallColor)
                {
                    algorithmToCorrectCross();
                } else
                {
                    cube.U();
                }
            } else
            {
                algorithmToCorrectCross();
            }
            composeCorrectCross();
        }


        private int match(Label.Color[] walls,Label.Color[] blocks,int offset)
        {
            int match = 0;
            for (int i = 0; i < 4; i++)
            {
                if (blocks[i+offset % 4] == walls[i % 4])
                {
                    match++;
                }
            }
            return match;
        }



        private void algorithmToCorrectCross()
        {
            cube.R().U().RPrim().U().R().U().U().RPrim().U();
        }



    }
}
