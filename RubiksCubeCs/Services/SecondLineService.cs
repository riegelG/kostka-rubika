using System;
using System.Collections.Generic;
using System.Linq;
using RubiksCubeCs.Models;

namespace RubiksCubeCs.Services
{
    public class SecondLineService
    {
        private Cube cube;

        public SecondLineService(Cube cube)
        {
            this.cube = cube;
        }

        public void compose()
        {
            cube.XPrim();

            Label.Color front = cube.GetWallColor(Cube.WallType.Front);
            Label.Color left = cube.GetWallColor(Cube.WallType.Left);
            Block blockL = cube.FindBlock(new Label.Color[] { front, left });
            composeLeftEdge(front, left, blockL);


            Label.Color right = cube.GetWallColor(Cube.WallType.Right);
            Block blockR = cube.FindBlock(new Label.Color[] { front, right });
            composeRightEdge(front, right, blockR);

            cube.Y().Y();

            Label.Color back = cube.GetWallColor(Cube.WallType.Front);
            Label.Color leftB = cube.GetWallColor(Cube.WallType.Left);
            Block blockLB = cube.FindBlock(new Label.Color[] { back, leftB });
            composeLeftEdge(back, leftB, blockLB);


            Label.Color rightB = cube.GetWallColor(Cube.WallType.Right);
            Block blockRB = cube.FindBlock(new Label.Color[] { back, rightB });
            composeRightEdge(back, rightB, blockRB);
        }


        private void composeLeftEdge(Label.Color front, Label.Color left, Block block)
        {
            List<Label> labels = block.GetLabels();
            Cube.WallType frontPosition = labels.Where((b) => b.color == front).Select((b) => b.position).First();
            Cube.WallType leftPosition = labels.Where((b) => b.color == left).Select((b) => b.position).First();

            switch (frontPosition, leftPosition)
            {
                case (Cube.WallType.Front, Cube.WallType.Left): break;
                case (Cube.WallType.Left, Cube.WallType.Front):
                    leftRotate();
                    break;
                case (Cube.WallType.Front, Cube.WallType.Up):
                    toLeft();
                    break;
                case (Cube.WallType.Up, _):
                    cube.YPrim();
                    composeRightEdge(left, front, block);
                    cube.Y();
                    break;
                case (_, Cube.WallType.Up):
                    cube.U();
                    composeLeftEdge(front, left, block);
                    break;
                default:
                    removeLeft(frontPosition, leftPosition);
                    composeRightEdge(left, front, block);
                    break;
            }
        }

        private void composeRightEdge(Label.Color front, Label.Color right, Block block)
        {
            List<Label> labels = block.GetLabels();
            Cube.WallType frontPosition = labels.Where((b) => b.color == front).Select((b) => b.position).First();
            Cube.WallType rightPosition = labels.Where((b) => b.color == right).Select((b) => b.position).First();

            switch (frontPosition, rightPosition)
            {
                case (Cube.WallType.Front, Cube.WallType.Right): break;
                case (Cube.WallType.Right, Cube.WallType.Front):
                    rightRotate();
                    break;
                case (Cube.WallType.Front, Cube.WallType.Up):
                    toRight();
                    break;
                case (Cube.WallType.Up, _):
                    cube.Y();
                    composeLeftEdge(right, front, block);
                    cube.YPrim();
                    break;
                case (_, Cube.WallType.Up):
                    cube.U();
                    composeRightEdge(front, right, block);
                    break;
                default:
                    removeLeft(frontPosition, rightPosition);
                    composeRightEdge(right, front, block);
                    break;
            }
        }

        private void removeLeft(Cube.WallType front, Cube.WallType left)
        {
            switch (front, left)
            {
                case (Cube.WallType.Left, Cube.WallType.Front):
                    toLeft();
                    break;
                default:
                    cube.YPrim();
                    removeLeft(front, left);
                    cube.Y();
                    break;
            }
        }

        private void removeRight(Cube.WallType front, Cube.WallType right)
        {
            switch (front, right)
            {
                case (Cube.WallType.Right, Cube.WallType.Front):
                    toRight();
                    break;
                default:
                    cube.Y();
                    removeRight(front, right);
                    cube.YPrim();
                    break;

            }
        }

        private void toRight()
        {
            cube.U().R().UPrim().RPrim().UPrim().FPrim().U().F();
        }

        private void toLeft()
        {
            cube.UPrim().LPrim().U().L().U().F().UPrim().FPrim();
        }

        private void leftRotate()
        {
            toLeft();
            cube.U().U();
            toLeft();
        }

        private void rightRotate()
        {
            toRight();
            cube.U().U();
            toRight();
        }
    }
}
